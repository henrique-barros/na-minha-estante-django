from django import forms

from .models import Comment


class PostForm(forms.Form):
    name = forms.CharField(label='Nome', max_length=255)
    author = forms.CharField(label='Autor', max_length=255)
    publish_year = forms.IntegerField(label='Ano de Publicação')
    cover_url = forms.URLField(label='URL da Capa', max_length=200)

    review_score = forms.IntegerField(label="Avaliação (1-5)", min_value=1, max_value=5)
    content = forms.CharField(label="Conteúdo (em HTML)", max_length=10000)

class CommentForm(forms.ModelForm):
    class Meta:
        model = Comment
        fields = [
            'author',
            'text',
        ]
        labels = {
            'author': 'Usuário',
            'text': 'Comentário',
        }