# Generated by Django 3.2.7 on 2021-11-18 17:48

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('resenhas', '0003_comment_post'),
    ]

    operations = [
        migrations.CreateModel(
            name='Category',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=20, verbose_name='Nome')),
                ('description', models.CharField(max_length=255, verbose_name='Descrição')),
                ('posts', models.ManyToManyField(to='resenhas.Post', verbose_name='Resenhas')),
            ],
        ),
    ]
