from datetime import datetime
from django.shortcuts import get_object_or_404, render
from django.http.response import HttpResponseRedirect
from django.urls.base import reverse, reverse_lazy
from django.views import generic

from .models import Category, Comment, Post
from .forms import CommentForm


class PostListView(generic.ListView):
    model = Post
    template_name = 'resenhas/index.html'

class PostDetailView(generic.DetailView):
    model = Post
    template_name = 'resenhas/detail.html'

class PostCreateView(generic.CreateView):
    model = Post
    template_name = 'resenhas/create.html'
    fields = [
        'name',
        'author',
        'publish_year',
        'cover_url',
        'review_score',
        'post_date',
        'content',
    ]

    def get_success_url(self):
        return reverse('resenhas:detail', kwargs={'pk': self.object.pk})

class PostUpdateView(generic.UpdateView):
    model = Post
    template_name = 'resenhas/update.html'
    fields = [
        'name',
        'author',
        'publish_year',
        'cover_url',
        'review_score',
        'post_date',
        'content',
    ]
    
    def get_success_url(self):
        return reverse('resenhas:detail', kwargs={'pk': self.object.pk})

class PostDeleteView(generic.DeleteView):
    model = Post
    template_name = 'resenhas/delete.html'
    success_url = reverse_lazy('resenhas:index')

def create_comment(request, post_id):
    post = get_object_or_404(Post, pk=post_id)
    if request.method == 'POST':
        form = CommentForm(request.POST)
        if form.is_valid():
            author = form.cleaned_data['author']
            text = form.cleaned_data['text']
            post_date = datetime.now()
            comment = Comment(
                author=author,
                text=text,
                post_date=post_date,
                post=post
            )
            comment.save()
            return HttpResponseRedirect(reverse('resenhas:detail', args=(post_id,)))
    else:
        form = CommentForm()
    context = {'form': form, 'post': post}
    return render(request, 'resenhas/comment.html', context)

class CategoryListView(generic.ListView):
    model = Category
    template_name = 'resenhas/categories.html'

def detail_category(request, category_id):
    category = get_object_or_404(Category, pk=category_id)
    context = {'category': category, 'post_list': category.posts.all()}
    return render(request, 'resenhas/index.html', context)