from django.db import models
from django.conf import settings


class Post(models.Model):
    name = models.CharField(max_length=255, verbose_name="Título")
    author = models.CharField(max_length=255, verbose_name="Autor")
    publish_year = models.IntegerField(verbose_name="Ano de Publicação")
    cover_url = models.URLField(max_length=200, null=True, verbose_name="URL da Capa")
    
    content = models.CharField(max_length=10000, verbose_name="Conteúdo")
    review_score = models.IntegerField(verbose_name="Avaliação")
    post_date = models.DateTimeField(verbose_name="Data de Postagem")

    def __str__(self):
        return f"{self.name}, de {self.author}"

class Comment(models.Model):
    author = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
        verbose_name="Autor"
    )
    text = models.CharField(max_length=255, verbose_name="Texto")
    post_date = models.DateTimeField(verbose_name="Data de Postagem")
    post = models.ForeignKey(Post, on_delete=models.CASCADE, verbose_name="Resenha")

    def __str__(self):
        return f'"{self.text}", {self.author}'

class Category(models.Model):
    name = models.CharField(max_length=20, verbose_name="Nome")
    description = models.CharField(max_length=255, verbose_name="Descrição")
    posts = models.ManyToManyField(Post, verbose_name="Resenhas")

    def __str__(self):
        return f'{self.name}'